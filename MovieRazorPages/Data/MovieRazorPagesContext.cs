﻿using Microsoft.EntityFrameworkCore;
using MovieRazorPages.Models;

namespace MovieRazorPages.Data
{
    public class MovieRazorPagesContext : DbContext
    {
        public MovieRazorPagesContext (DbContextOptions<MovieRazorPagesContext> options)
            : base(options)
        {
        }

        public DbSet<Movie> Movie { get; set; }
    }
}
