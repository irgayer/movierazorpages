﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MovieRazorPages.Data;
using MovieRazorPages.Models;

namespace MovieRazorPages.Pages.Movies
{
    public class DetailsModel : PageModel
    {
        private readonly MovieRazorPages.Data.MovieRazorPagesContext _context;

        public DetailsModel(MovieRazorPages.Data.MovieRazorPagesContext context)
        {
            _context = context;
        }

        public Movie Movie { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Movie = await _context.Movie.FirstOrDefaultAsync(m => m.ID == id);

            if (Movie == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
