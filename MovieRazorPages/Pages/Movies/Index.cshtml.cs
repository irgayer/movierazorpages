﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MovieRazorPages.Data;
using MovieRazorPages.Models;

namespace MovieRazorPages.Pages.Movies
{
    public class IndexModel : PageModel
    {
        private readonly MovieRazorPages.Data.MovieRazorPagesContext _context;

        public IndexModel(MovieRazorPages.Data.MovieRazorPagesContext context)
        {
            _context = context;
        }

        public IList<Movie> Movie { get;set; }

        public async Task OnGetAsync()
        {
            Movie = await _context.Movie.ToListAsync();
        }
    }
}
